package com.br.stefanini.sebo.persistence.DTO;

import java.io.Serializable;

public class AutorSelecaoDTO implements Serializable {

    private static final long serialVersionUID = 2452172112016777942L;

    private Long id;

    private String nome;

    public AutorSelecaoDTO(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
