package com.br.stefanini.sebo.persistence.DTO;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

public class ObraCadastroDTO implements Serializable {

    private static final long serialVersionUID = -4852662448341693087L;

    @NotNull(message = "O campo nome não pode ser nulo")
    @NotEmpty(message = "O campo nome não pode ser vázio")
    private String nome;

    @NotNull(message = "O campo descricao não pode ser nulo")
    @NotEmpty(message = "O campo descricao não pode ser vázio")
    @Size(max = 240, message = "O campo descricao não pode exceder 240 caracteres")
    private String descricao;

    private LocalDate dataPublicacao;

    private LocalDate dataExposicao;

    @NotNull(message = "o campo imagem não pode ser nulo")
    private MultipartFile imagem;

    private Collection<Long> autores;

    public ObraCadastroDTO(@NotNull(message = "O campo nome não pode ser nulo")
                   @NotEmpty(message = "O campo nome não pode ser vázio") String nome,
                           @NotNull(message = "O campo descricao não pode ser nulo")
                   @NotEmpty(message = "O campo descricao não pode ser vázio")
                   @Size(max = 240, message = "O campo descricao não pode exceder 240 caracteres") String descricao,
                           LocalDate dataPublicacao, LocalDate dataExposicao,
                           @NotNull(message = "o campo imagem não pode ser nulo") MultipartFile imagem, Collection<Long> autores) {
        this.nome = nome;
        this.descricao = descricao;
        this.dataPublicacao = dataPublicacao;
        this.dataExposicao = dataExposicao;
        this.imagem = imagem;
        this.autores = autores;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(LocalDate dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public LocalDate getDataExposicao() {
        return dataExposicao;
    }

    public void setDataExposicao(LocalDate dataExposicao) {
        this.dataExposicao = dataExposicao;
    }

    public MultipartFile getImagem() {
        return imagem;
    }

    public void setImagem(MultipartFile imagem) {
        this.imagem = imagem;
    }

    public Collection<Long> getAutores() {
        return autores;
    }

    public void setAutores(Collection<Long> autores) {
        this.autores = autores;
    }
}
