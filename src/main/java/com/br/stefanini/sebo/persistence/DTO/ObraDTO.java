package com.br.stefanini.sebo.persistence.DTO;

import org.springframework.core.io.InputStreamResource;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

public class ObraDTO implements Serializable {

    private static final long serialVersionUID = -2394376039521175518L;

    private String nome;

    private String descricao;

    private LocalDate dataPublicacao;

    private LocalDate dataExposicao;

    private Collection<Long> autores;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(LocalDate dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public LocalDate getDataExposicao() {
        return dataExposicao;
    }

    public void setDataExposicao(LocalDate dataExposicao) {
        this.dataExposicao = dataExposicao;
    }

    public Collection<Long> getAutores() {
        return autores;
    }

    public void setAutores(Collection<Long> autores) {
        this.autores = autores;
    }
}
