package com.br.stefanini.sebo.persistence.repository;

import com.br.stefanini.sebo.persistence.DTO.AutorSelecaoDTO;
import com.br.stefanini.sebo.persistence.entities.Autor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long> {

    Boolean existsByCpf(String cpf);

    Boolean existsByEmail(String email);

    Boolean existsByCpfAndIdNot(String cpf, Long id);

    Boolean existsByEmailAndIdNot(String email, Long id);

    @Query("SELECT NEW com.br.stefanini.sebo.persistence.DTO.AutorSelecaoDTO(a.id, a.nome) FROM Autor a ")
    Collection<AutorSelecaoDTO> consularParaSelecao();

    Collection<Autor> findAllByIdIn(Collection<Long> id);
}
