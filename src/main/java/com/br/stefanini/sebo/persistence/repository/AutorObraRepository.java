package com.br.stefanini.sebo.persistence.repository;

import com.br.stefanini.sebo.persistence.entities.Autor;
import com.br.stefanini.sebo.persistence.entities.AutorObra;
import com.br.stefanini.sebo.persistence.entities.Obra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface AutorObraRepository extends JpaRepository<AutorObra, Long>, JpaSpecificationExecutor<AutorObra> {

    @Query("SELECT CASE WHEN (count(ao) > 0) THEN TRUE ELSE FALSE END FROM AutorObra ao WHERE ao.autor.id = :idAutor")
    Boolean existsByAutor(@Param("idAutor") Long idAutor);

    @Query("SELECT ao.autor FROM AutorObra ao WHERE ao.obra.id = :idObra")
    Collection<Autor> consultarAutores(@Param("idObra") Long idObra);

    @Query("SELECT ao.obra FROM AutorObra ao WHERE ao.autor.id = :idAutor")
    Collection<Obra> consultarObras(@Param("idAutor") Long idAutor);

    @Query("SELECT ao FROM AutorObra ao JOIN ao.obra o WHERE ao.autor.id = :idAutor")
    Optional<AutorObra> consultarPorAutor(@Param("idAutor") Long idAutor);

    @Query("SELECT ao FROM AutorObra ao JOIN ao.autor a WHERE ao.obra.id = :idObra")
    Optional<AutorObra> consultarPorObra(@Param("idObra") Long idObra);

    void deleteAllByAutor(Autor autor);

    Collection<Long> findAllByAutor(Autor autor);

    void deleteAllByObra(Obra obra);
}
