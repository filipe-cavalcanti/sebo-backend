package com.br.stefanini.sebo.persistence.repository;

import com.br.stefanini.sebo.persistence.DTO.ObraSelecaoDTO;
import com.br.stefanini.sebo.persistence.entities.Obra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ObraRepository extends JpaRepository<Obra, Long>, JpaSpecificationExecutor<Obra> {

    @Query("SELECT NEW com.br.stefanini.sebo.persistence.DTO.ObraSelecaoDTO(o.id, o.nome) FROM Obra o")
    Collection<ObraSelecaoDTO> consultarParaSelecao();

    Collection<Obra> findAllByIdIn(Collection<Long> id);

}
