package com.br.stefanini.sebo.persistence.repository;

import com.br.stefanini.sebo.persistence.entities.Pais;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaisRepository extends JpaRepository<Pais, Long> {
}
