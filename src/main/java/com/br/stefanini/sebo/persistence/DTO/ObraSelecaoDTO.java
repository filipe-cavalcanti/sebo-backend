package com.br.stefanini.sebo.persistence.DTO;

import java.io.Serializable;

public class ObraSelecaoDTO implements Serializable {

    private static final long serialVersionUID = 7761966286975212068L;

    private Long id;

    private String nome;

    public ObraSelecaoDTO(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
