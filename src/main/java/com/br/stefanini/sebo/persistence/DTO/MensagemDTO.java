package com.br.stefanini.sebo.persistence.DTO;

import java.io.Serializable;

public class MensagemDTO implements Serializable {

    private static final long serialVersionUID = -9134349336913360230L;

    private String mensagem;

    public MensagemDTO(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
