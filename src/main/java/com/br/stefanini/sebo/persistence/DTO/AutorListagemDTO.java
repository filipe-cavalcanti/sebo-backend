package com.br.stefanini.sebo.persistence.DTO;

import java.io.Serializable;
import java.time.LocalDate;

public class AutorListagemDTO implements Serializable {

    private static final long serialVersionUID = 2393923757232662009L;

    private Long id;

    private String nome;

    private LocalDate dataNascimento;

    private String paisOrigem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getPaisOrigem() {
        return paisOrigem;
    }

    public void setPaisOrigem(String paisOrigem) {
        this.paisOrigem = paisOrigem;
    }
}
