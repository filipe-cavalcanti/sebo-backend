package com.br.stefanini.sebo.persistence.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "autor_obra")
@SequenceGenerator(name = "autor_obra_seq", sequenceName = "autor_obra_seq", allocationSize = 1)
public class AutorObra implements Serializable {

    private static final long serialVersionUID = -282142294938835010L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autor_obra_seq")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "fk_autor", foreignKey = @ForeignKey(name = "fk_autor"))
    private Autor autor;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "fk_obra", foreignKey = @ForeignKey(name = "fk_obra"))
    private Obra obra;

    public Long getId() {
        return id;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Obra getObra() {
        return obra;
    }

    public void setObra(Obra obra) {
        this.obra = obra;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AutorObra autorObra = (AutorObra) o;
        return Objects.equals(id, autorObra.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
