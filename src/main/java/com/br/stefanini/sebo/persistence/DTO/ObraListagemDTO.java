package com.br.stefanini.sebo.persistence.DTO;

import java.io.Serializable;
import java.time.LocalDate;

public class ObraListagemDTO implements Serializable {

    private static final long serialVersionUID = -3151225335278133565L;

    private Long id;

    private String nome;

    private String descricao;

    private LocalDate dataPublicacao;

    private LocalDate dataExposicao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(LocalDate dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public LocalDate getDataExposicao() {
        return dataExposicao;
    }

    public void setDataExposicao(LocalDate dataExposicao) {
        this.dataExposicao = dataExposicao;
    }
}
