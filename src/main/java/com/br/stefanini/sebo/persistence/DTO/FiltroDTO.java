package com.br.stefanini.sebo.persistence.DTO;

import java.io.Serializable;

public class FiltroDTO implements Serializable {

    private static final long serialVersionUID = -992871344625081391L;

    private String nomeObra;

    private String nomeAutor;

    private String descricaoObra;

    public String getNomeObra() {
        return nomeObra;
    }

    public void setNomeObra(String nomeObra) {
        this.nomeObra = nomeObra;
    }

    public String getNomeAutor() {
        return nomeAutor;
    }

    public void setNomeAutor(String nomeAutor) {
        this.nomeAutor = nomeAutor;
    }

    public String getDescricaoObra() {
        return descricaoObra;
    }

    public void setDescricaoObra(String descricaoObra) {
        this.descricaoObra = descricaoObra;
    }
}
