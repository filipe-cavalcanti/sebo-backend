package com.br.stefanini.sebo.persistence.DTO;

import com.br.stefanini.sebo.infraestrutura.RegexUtil;
import com.br.stefanini.sebo.util.SexoEnum;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

public class AutorDTO implements Serializable {

    private static final long serialVersionUID = 224626344657559399L;

    @NotNull(message = "O campo nome não pode ser nulo")
    @NotEmpty(message = "O campo nome não pode ser vázio")
    private String nome;

    private SexoEnum sexo;

    @Email(message = "O e-mail deve seguir o exemplo exemplo@com")
    private String email;

    @NotNull(message = "O campo dataNascimento não pode ser nulo")
    private LocalDate dataNascimento;

    private String cpf;

    @NotNull(message = "O campo paisOrigem não pode ser nulo")
    private Long paisOrigem;

    private Collection<Long> obras;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public SexoEnum getSexo() {
        return sexo;
    }

    public void setSexo(SexoEnum sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Collection<Long> getObras() {
        return obras;
    }

    public void setObras(Collection<Long> obras) {
        this.obras = obras;
    }

    public Long getPaisOrigem() {
        return paisOrigem;
    }

    public void setPaisOrigem(Long paisOrigem) {
        this.paisOrigem = paisOrigem;
    }
}
