package com.br.stefanini.sebo.service.implementation;

import com.br.stefanini.sebo.persistence.DTO.PaisSelecaoDTO;
import com.br.stefanini.sebo.persistence.repository.PaisRepository;
import com.br.stefanini.sebo.service.declaration.PaisService;
import com.br.stefanini.sebo.service.mapper.PaisMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PaisServiceImpl implements PaisService {

    private PaisRepository paisRepository;

    @Autowired
    public PaisServiceImpl(PaisRepository paisRepository) {
        this.paisRepository = paisRepository;
    }

    @Override
    public Collection<PaisSelecaoDTO> consultarParaSelecao() {
        return this.paisRepository.findAll().stream().map(PaisMapper::mapper).collect(Collectors.toList());
    }
}
