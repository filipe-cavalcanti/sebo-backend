package com.br.stefanini.sebo.service.implementation;

import com.br.stefanini.sebo.infraestrutura.RegexUtil;
import com.br.stefanini.sebo.infraestrutura.exceptions.BusinessException;
import com.br.stefanini.sebo.infraestrutura.mensagem.MensagemErro;
import com.br.stefanini.sebo.infraestrutura.mensagem.MensagemSucesso;
import com.br.stefanini.sebo.persistence.DTO.AutorDTO;
import com.br.stefanini.sebo.persistence.DTO.AutorListagemDTO;
import com.br.stefanini.sebo.persistence.DTO.AutorSelecaoDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;
import com.br.stefanini.sebo.persistence.entities.Autor;
import com.br.stefanini.sebo.persistence.entities.Obra;
import com.br.stefanini.sebo.persistence.entities.Pais;
import com.br.stefanini.sebo.persistence.repository.AutorRepository;
import com.br.stefanini.sebo.persistence.repository.ObraRepository;
import com.br.stefanini.sebo.persistence.repository.PaisRepository;
import com.br.stefanini.sebo.service.declaration.AutorObraService;
import com.br.stefanini.sebo.service.declaration.AutorService;
import com.br.stefanini.sebo.service.mapper.AutorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class AutorServiceImpl implements AutorService {

    private static final String BRASIL = "Brazil";

    private AutorRepository autorRepository;
    private AutorObraService autorObraService;
    private ObraRepository obraRepository;
    private PaisRepository paisRepository;


    @Autowired
    public AutorServiceImpl(AutorRepository autorRepository,
                            AutorObraService autorObraService,
                            ObraRepository obraRepository,
                            PaisRepository paisRepository) {
        this.autorRepository = autorRepository;
        this.autorObraService = autorObraService;
        this.obraRepository = obraRepository;
        this.paisRepository = paisRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO cadastrar(AutorDTO autorDTO) {

        this.processarEmail(autorDTO, this.autorRepository.existsByEmail(autorDTO.getEmail()));

        Pais pais = this.paisRepository.findById(autorDTO.getPaisOrigem())
                .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));

        if (!this.validarDocumentosBrasil(autorDTO, pais,() -> this.validarCPFCadastro(autorDTO))) {
            throw new BusinessException(MensagemErro.CPF_INCORRETO);
        }

        Autor autor = this.autorRepository.save(AutorMapper.mapper(autorDTO, pais));

        this.vincularObras(autorDTO.getObras(), autor);

        return new MensagemDTO(MensagemSucesso.CADASTRO_REALIZADO);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO editar(AutorDTO autorDTO, Long id) {
        Autor autor = this.autorRepository.findById(id).
                orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));

        Pais pais = this.paisRepository.findById(autorDTO.getPaisOrigem())
                .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));

        this.processarEmail(autorDTO, autorRepository.existsByEmailAndIdNot(autorDTO.getEmail(), id));

        if (!this.validarDocumentosBrasil(autorDTO, pais, () -> this.validarCPFEdicao(autorDTO, id))) {
            throw new BusinessException(MensagemErro.CPF_INCORRETO);
        }

        autor = AutorMapper.mapper(autorDTO, autor, pais);

        this.autorObraService.atualizarVinculos(autor, autorDTO.getObras());

        return new MensagemDTO(MensagemSucesso.EDICAO_REALIZADA);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO remover(Long id) {
        if (this.autorObraService.existeObraVinculada(id)) {
            throw new BusinessException(MensagemErro.EXCLUSAO_AUTOR_NAO_PERMITIDA);
        }
        this.autorRepository.deleteById(id);
        return new MensagemDTO(MensagemSucesso.REMOCAO_REALIZADA);
    }

    @Override
    public AutorDTO consultarPorId(Long id) {
        Autor autor = this.autorRepository.findById(id)
                .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));
        Collection<Long> obras = this.autorObraService.consultarObras(id).stream().map(Obra::getId).collect(Collectors.toList());
        return AutorMapper.mapper(autor, obras);
    }

    @Override
    public Collection<AutorListagemDTO> consultar() {
        return this.autorRepository.findAll().stream().map(AutorMapper::mapperListagem).collect(Collectors.toList());
    }

    @Override
    public Collection<AutorSelecaoDTO> consultarParaSelecao() {
        return this.autorRepository.consularParaSelecao();
    }

    private Boolean validarDocumentosBrasil(AutorDTO autorDTO, Pais pais, Supplier<Boolean> supplier) {
        Boolean isValido = Boolean.TRUE;
        if (pais.getNome().equals(BRASIL)) {
            if (!supplier.get()) {
                isValido = Boolean.FALSE;
            }
        }
        else {
            autorDTO.setCpf(null);
        }
        return isValido;
    }

    private void vincularObras(Collection<Long> idsObras, Autor autor) {
        idsObras.forEach(idObra -> {
            Obra obra = this.obraRepository.findById(idObra)
                    .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));
            this.autorObraService.vincular(autor, obra);
        });
    }

    private Boolean validarCPFCadastro(AutorDTO autorDTO) {
        Boolean isValid = Boolean.FALSE;
        if (autorDTO.getCpf() != null && autorDTO.getCpf().matches(RegexUtil.CPF) && !this.autorRepository.existsByCpf(autorDTO.getCpf())) {
            isValid = Boolean.TRUE;
        }
        return isValid;
    }

    private Boolean validarCPFEdicao(AutorDTO autorDTO, Long id) {
        Boolean isValid = Boolean.FALSE;
        if (autorDTO.getCpf() != null && autorDTO.getCpf().matches(RegexUtil.CPF) && !this.autorRepository.existsByCpfAndIdNot(autorDTO.getCpf(), id)) {
            isValid = Boolean.TRUE;
        }
        return isValid;
    }

    private void processarEmail(AutorDTO autorDTO, Boolean existeNaBase) {
        if(autorDTO.getEmail() != null) {
            if (!autorDTO.getEmail().isEmpty() && existeNaBase) {
                throw new BusinessException(MensagemErro.EMAIL_EXISTE);
            }
            else if (autorDTO.getEmail().isEmpty()) {
                autorDTO.setEmail(null);
            }
        }
    }
}
