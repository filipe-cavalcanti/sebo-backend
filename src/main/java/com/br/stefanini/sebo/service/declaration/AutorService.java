package com.br.stefanini.sebo.service.declaration;

import com.br.stefanini.sebo.persistence.DTO.AutorDTO;
import com.br.stefanini.sebo.persistence.DTO.AutorListagemDTO;
import com.br.stefanini.sebo.persistence.DTO.AutorSelecaoDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;

import java.util.Collection;

/**
 * Interface service para a o gerenciamento dos autores
 * @author felicavalcanti157@gmail.com
 */
public interface AutorService {

    /**
     * Método de cadastro de ator
     * @param autor - DTO para o autor
     * @return {@link MensagemDTO} - Mensagem de retorno do método
     */
    MensagemDTO cadastrar(AutorDTO autor);

    MensagemDTO editar(AutorDTO autor, Long id);

    MensagemDTO remover(Long id);

    AutorDTO consultarPorId(Long id);

    Collection<AutorListagemDTO> consultar();

    Collection<AutorSelecaoDTO> consultarParaSelecao();
}
