package com.br.stefanini.sebo.service.mapper;

import com.br.stefanini.sebo.persistence.DTO.ObraCadastroDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraListagemDTO;
import com.br.stefanini.sebo.persistence.entities.Autor;
import com.br.stefanini.sebo.persistence.entities.Obra;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ObraMapper {

    public static Obra mapper(ObraCadastroDTO obraCadastroDTO, String urlImagem) {
        Obra obra = new Obra();
        obra.setDataExposicao(obraCadastroDTO.getDataExposicao());
        obra.setDataPublicacao(obraCadastroDTO.getDataPublicacao());
        obra.setDescricao(obraCadastroDTO.getDescricao());
        obra.setNome(obraCadastroDTO.getNome());
        obra.setUrlImagem(urlImagem);
        return obra;
    }

    public static ObraCadastroDTO mapper(MultipartFile imagem, String nome,
                                         String descricao, String dataPublicacaoString,
                                         String dataExposicaoString, String autores) {
        LocalDate dataPublicacao;
        LocalDate dataExposicao;

        dataPublicacaoString = dataPublicacaoString.replace("\"", "");
        dataExposicaoString = dataExposicaoString.replace("\"", "");

        dataPublicacao = ObraMapper.mapper(dataPublicacaoString);
        dataExposicao = ObraMapper.mapper(dataExposicaoString);

        return new ObraCadastroDTO(nome, descricao, dataPublicacao, dataExposicao, imagem, ObraMapper.parseAutores(autores));
    }

    public static ObraListagemDTO mapper(Obra obra) {
        ObraListagemDTO obraListagemDTO = new ObraListagemDTO();
        obraListagemDTO.setId(obra.getId());
        obraListagemDTO.setDataExposicao(obra.getDataExposicao());
        obraListagemDTO.setDataPublicacao(obra.getDataPublicacao());
        obraListagemDTO.setDescricao(obra.getDescricao());
        obraListagemDTO.setNome(obra.getNome());
        return obraListagemDTO;
    }

    public static ObraDTO mapper(Obra obra, Collection<Autor> autores) {
        ObraDTO obraDTO = new ObraDTO();
        obraDTO.setNome(obra.getNome());
        obraDTO.setDataExposicao(obra.getDataExposicao());
        obraDTO.setDataPublicacao(obra.getDataPublicacao());
        obraDTO.setDescricao(obra.getDescricao());
        obraDTO.setAutores(autores.stream().map(Autor::getId).collect(Collectors.toList()));
        return obraDTO;
    }

    private static LocalDate mapper(String dateString) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = null;
        if (!dateString.isEmpty()) {
            try {
                date = LocalDate.parse(dateString, dateTimeFormatter);
            } catch (DateTimeParseException e) {
                date = LocalDate.parse(dateString, dateFormatter);
            }
        }
        return date;
    }

    public static Obra mapper(ObraCadastroDTO obraCadastroDTO, Obra obra, String imagemUrl) {
        obra.setNome(obraCadastroDTO.getNome());
        obra.setUrlImagem(imagemUrl);
        obra.setDescricao(obraCadastroDTO.getDescricao());
        obra.setDataPublicacao(obraCadastroDTO.getDataPublicacao());
        obra.setDataExposicao(obraCadastroDTO.getDataExposicao());
        return obra;
    }

    private static Collection<Long> parseAutores(String autores) {
        autores = autores.replace("[", "").replace("]", "");
        return Arrays.stream(autores.isEmpty() ? new String[0] : autores.split(","))
                .map(Long::new).collect(Collectors.toList());
    }
}
