package com.br.stefanini.sebo.service.declaration;

import com.br.stefanini.sebo.persistence.DTO.PaisSelecaoDTO;

import java.util.Collection;

public interface PaisService {

    Collection<PaisSelecaoDTO> consultarParaSelecao();
}
