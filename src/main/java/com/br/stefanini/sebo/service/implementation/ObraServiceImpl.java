package com.br.stefanini.sebo.service.implementation;

import com.br.stefanini.sebo.infraestrutura.dropbox.DropBoxAdapter;
import com.br.stefanini.sebo.infraestrutura.exceptions.BusinessException;
import com.br.stefanini.sebo.infraestrutura.mensagem.MensagemErro;
import com.br.stefanini.sebo.infraestrutura.mensagem.MensagemSucesso;
import com.br.stefanini.sebo.persistence.DTO.FiltroDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraCadastroDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraListagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraSelecaoDTO;
import com.br.stefanini.sebo.persistence.entities.Autor;
import com.br.stefanini.sebo.persistence.entities.Obra;
import com.br.stefanini.sebo.persistence.repository.AutorRepository;
import com.br.stefanini.sebo.persistence.repository.ObraRepository;
import com.br.stefanini.sebo.service.declaration.AutorObraService;
import com.br.stefanini.sebo.service.declaration.ObraService;
import com.br.stefanini.sebo.service.mapper.ObraMapper;
import com.dropbox.core.DbxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class ObraServiceImpl implements ObraService {

    private ObraRepository obraRepository;

    private AutorObraService autorObraService;

    private DropBoxAdapter dropBoxAdapter;

    private AutorRepository autorRepository;

    @Autowired
    public ObraServiceImpl(ObraRepository obraRepository,
                           AutorObraService autorObraService,
                           DropBoxAdapter dropBoxAdapter,
                           AutorRepository autorRepository) {
        this.obraRepository = obraRepository;
        this.autorObraService = autorObraService;
        this.dropBoxAdapter = dropBoxAdapter;
        this.autorRepository = autorRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO cadastrar(ObraCadastroDTO obraCadastroDTO) {
        Obra obra = this.obraRepository.save(this.buildObra(obraCadastroDTO,imagemUrl -> ObraMapper.mapper(obraCadastroDTO, imagemUrl)));
        this.autorObraService.vincular(this.autorRepository.findAllByIdIn(obraCadastroDTO.getAutores()), obra);
        return new MensagemDTO(MensagemSucesso.CADASTRO_REALIZADO);
    }

    @Override
    public Long consultarQuantidade(FiltroDTO filtroDTO) {
        Long quantidadeRegistros;
        if (filtroDTO.getNomeAutor() != null && !filtroDTO.getNomeAutor().isEmpty()) {
            quantidadeRegistros = this.autorObraService.consultarQuantidadeObras(filtroDTO);
        }
        else {
            quantidadeRegistros = this.obraRepository.count(this.buildSpecificationConsultaObras(filtroDTO));
        }
        return quantidadeRegistros;
    }

    @Override
    public Page<ObraListagemDTO> consultar(FiltroDTO filtroDTO, int pageIndex, int size) {
        Pageable pageable = PageRequest.of(pageIndex, size);
        Page<ObraListagemDTO> obraListagemDTOPage;
        if(filtroDTO.getNomeAutor() != null && !filtroDTO.getNomeAutor().isEmpty()) {
            obraListagemDTOPage = this.autorObraService.consultarObrasPorFiltro(filtroDTO, pageable);
        }
        else {
            obraListagemDTOPage = new PageImpl<>(this.obraRepository
                    .findAll(this.buildSpecificationConsultaObras(filtroDTO), pageable)
                    .stream().map(ObraMapper::mapper).collect(Collectors.toList()));
        }
        return obraListagemDTOPage;
    }

    @Override
    public Collection<ObraSelecaoDTO> consultarParaSelecao() {
        return this.obraRepository.consultarParaSelecao();
    }

    @Override
    public ObraDTO consultarPorId(Long id) {
        Obra obra = this.obraRepository.findById(id)
                .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));
        Collection<Autor> autores = this.autorObraService.consultarAutores(id);
        return ObraMapper.mapper(obra, autores);
    }

    @Override
    public byte[] consultarImagem(Long id) {
        Obra obra = this.obraRepository.findById(id)
                .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));
        try {
            return this.dropBoxAdapter.download(obra.getUrlImagem());
        } catch (IOException | DbxException e) {
            throw new BusinessException(MensagemErro.ERRO_DOWNLOAD_IMAGEM);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO editar(ObraCadastroDTO obraCadastroDTO, Long id) {
        Obra obra = this.obraRepository.findById(id)
                .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));
        this.autorObraService.removerTodosPorObra(obra);
        this.autorObraService.vincular(this.autorRepository.findAllByIdIn(obraCadastroDTO.getAutores()), obra);
        this.obraRepository.save(buildObra(obraCadastroDTO, imagemUrl -> ObraMapper.mapper(obraCadastroDTO, obra, imagemUrl)));
        return new MensagemDTO(MensagemSucesso.EDICAO_REALIZADA);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO remover(Long id) {
        Obra obra = obraRepository.findById(id)
                .orElseThrow(() -> new BusinessException(MensagemErro.NEMHUM_REGISTRO_ENCONTRADO));
        this.autorObraService.removerTodosPorObra(obra);
        this.obraRepository.delete(obra);
        return new MensagemDTO(MensagemSucesso.REMOCAO_REALIZADA);
    }

    private Obra buildObra(ObraCadastroDTO obraCadastroDTO, Function<String, Obra> mapper) {
        String imageUrl;
        try {
            imageUrl = this.dropBoxAdapter.upload(obraCadastroDTO.getImagem());
        } catch (IOException | DbxException e) {
            throw new BusinessException(MensagemErro.ERRO_UPLOAD_IMAGEM);
        }
        if (!this.validarDatas(obraCadastroDTO)) {
            throw new BusinessException(MensagemErro.ERRO_DATAS_OBRA);
        }
        return mapper.apply(imageUrl);
    }

    private Boolean validarDatas(ObraCadastroDTO obraCadastroDTO) {
        Boolean isValid = Boolean.TRUE;
        if (obraCadastroDTO.getDataExposicao() == null && obraCadastroDTO.getDataPublicacao() == null) {
            isValid = Boolean.FALSE;
        }
        return isValid;
    }

    private Specification<Obra> buildSpecificationConsultaObras(FiltroDTO filtroDTO) {
        return (Specification<Obra>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filtroDTO.getNomeObra() != null) {
                predicates.add(criteriaBuilder.like(root.get("nome"), "%".concat(filtroDTO.getNomeObra().concat("%"))));
            }
            if (filtroDTO.getDescricaoObra() != null) {
                predicates.add(criteriaBuilder.like(root.get("descricao"), "%".concat(filtroDTO.getDescricaoObra().concat("%"))));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
