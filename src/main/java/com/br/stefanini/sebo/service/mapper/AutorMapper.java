package com.br.stefanini.sebo.service.mapper;

import com.br.stefanini.sebo.persistence.DTO.AutorDTO;
import com.br.stefanini.sebo.persistence.DTO.AutorListagemDTO;
import com.br.stefanini.sebo.persistence.entities.Autor;
import com.br.stefanini.sebo.persistence.entities.Pais;

import java.util.Collection;

/**
 * Mapper para o recurso Autor
 * @author felicavalcanti157@gmail.com
 */
public class AutorMapper {

    public static Autor mapper(AutorDTO autorDTO, Pais pais) {
        Autor autor = new Autor();
        return baseMapper(autorDTO, autor, pais);
    }

    public static Autor mapper(AutorDTO autorDTO, Autor autor, Pais pais) {
        return baseMapper(autorDTO, autor, pais);
    }

    private static Autor baseMapper(AutorDTO autorDTO, Autor autor, Pais pais) {
        autor.setCpf(autorDTO.getCpf());
        autor.setDataNascimento(autorDTO.getDataNascimento());
        autor.setEmail(autorDTO.getEmail());
        autor.setNome(autorDTO.getNome());
        autor.setPaisOrigem(pais);
        autor.setSexo(autorDTO.getSexo());
        return autor;
    }

    public static AutorDTO mapper(Autor autor, Collection<Long> idObras) {
        AutorDTO autorDTO = new AutorDTO();
        autorDTO.setCpf(autor.getCpf());
        autorDTO.setNome(autor.getNome());
        autorDTO.setSexo(autor.getSexo());
        autorDTO.setEmail(autor.getEmail());
        autorDTO.setDataNascimento(autor.getDataNascimento());
        autorDTO.setPaisOrigem(autor.getPaisOrigem().getId());
        autorDTO.setObras(idObras);
        return autorDTO;
    }

    public static AutorListagemDTO mapperListagem (Autor autor) {
        AutorListagemDTO autorListagemDTO = new AutorListagemDTO();
        autorListagemDTO.setDataNascimento(autor.getDataNascimento());
        autorListagemDTO.setId(autor.getId());
        autorListagemDTO.setNome(autor.getNome());
        autorListagemDTO.setPaisOrigem(autor.getPaisOrigem().getNome());
        return autorListagemDTO;
    }
}
