package com.br.stefanini.sebo.service.declaration;

import com.br.stefanini.sebo.persistence.DTO.FiltroDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraCadastroDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraListagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraSelecaoDTO;
import com.dropbox.core.DbxException;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.Collection;

public interface ObraService {

    MensagemDTO cadastrar(ObraCadastroDTO obra);

    Long consultarQuantidade(FiltroDTO filtroDTO);

    Page<ObraListagemDTO> consultar(FiltroDTO filtroDTO, int pageIndex, int size);

    Collection<ObraSelecaoDTO> consultarParaSelecao();

    ObraDTO consultarPorId(Long id) throws IOException, DbxException;

    byte[] consultarImagem(Long id);

    MensagemDTO editar(ObraCadastroDTO obra, Long id);

    MensagemDTO remover(Long id);
}
