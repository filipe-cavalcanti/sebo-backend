package com.br.stefanini.sebo.service.mapper;

import com.br.stefanini.sebo.persistence.DTO.PaisSelecaoDTO;
import com.br.stefanini.sebo.persistence.entities.Pais;

public class PaisMapper {

    public static PaisSelecaoDTO mapper(Pais pais) {
        PaisSelecaoDTO paisSelecaoDTO = new PaisSelecaoDTO();
        paisSelecaoDTO.setId(pais.getId());
        paisSelecaoDTO.setCodigo(pais.getCodigo());
        paisSelecaoDTO.setNome(pais.getNome());
        return paisSelecaoDTO;
    }
}
