package com.br.stefanini.sebo.service.declaration;

import com.br.stefanini.sebo.persistence.DTO.FiltroDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraListagemDTO;
import com.br.stefanini.sebo.persistence.entities.Autor;
import com.br.stefanini.sebo.persistence.entities.Obra;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

public interface AutorObraService {

    MensagemDTO vincular(Autor autor, Obra obra);

    MensagemDTO vincular(Collection<Autor> autores, Obra obra);

    Boolean existeObraVinculada(Long idAutor);

    Collection<Autor> consultarAutores(Long idObra);

    Collection<Obra> consultarObras(Long idAutor);

    MensagemDTO atualizarVinculos(Autor autor, Collection<Long> idsObras);

    Page<ObraListagemDTO> consultarObrasPorFiltro(FiltroDTO filtroDTO, Pageable pageable);

    Long consultarQuantidadeObras(FiltroDTO filtroDTO);

    void removerTodosPorObra(Obra obra);
}
