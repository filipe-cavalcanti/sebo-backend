package com.br.stefanini.sebo.service.implementation;

import com.br.stefanini.sebo.infraestrutura.mensagem.MensagemSucesso;
import com.br.stefanini.sebo.persistence.DTO.FiltroDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraListagemDTO;
import com.br.stefanini.sebo.persistence.entities.Autor;
import com.br.stefanini.sebo.persistence.entities.AutorObra;
import com.br.stefanini.sebo.persistence.entities.Obra;
import com.br.stefanini.sebo.persistence.repository.AutorObraRepository;
import com.br.stefanini.sebo.persistence.repository.ObraRepository;
import com.br.stefanini.sebo.service.declaration.AutorObraService;
import com.br.stefanini.sebo.service.mapper.ObraMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class AutorObraServiceImpl implements AutorObraService {

    private AutorObraRepository autorObraRepository;
    private ObraRepository obraRepository;

    @Autowired
    public AutorObraServiceImpl(AutorObraRepository autorObraRepository, ObraRepository obraRepository) {
        this.autorObraRepository = autorObraRepository;
        this.obraRepository = obraRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO vincular(Autor autor, Obra obra) {
        AutorObra autorObra = new AutorObra();
        autorObra.setAutor(autor);
        autorObra.setObra(obra);
        this.autorObraRepository.save(autorObra);
        return new MensagemDTO(MensagemSucesso.CADASTRO_REALIZADO);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO vincular(Collection<Autor> autores, Obra obra) {
        Collection<AutorObra> autorObras = autores.stream().map(autor -> {
            AutorObra autorObra = new AutorObra();
            autorObra.setAutor(autor);
            autorObra.setObra(obra);
            return autorObra;
        }).collect(Collectors.toList());
        this.autorObraRepository.saveAll(autorObras);
        return new MensagemDTO(MensagemSucesso.CADASTRO_REALIZADO);
    }

    @Override
    public Boolean existeObraVinculada(Long idAutor) {
        return this.autorObraRepository.existsByAutor(idAutor);
    }

    @Override
    public Collection<Autor> consultarAutores(Long idObra) {
        return this.autorObraRepository.consultarAutores(idObra);
    }

    @Override
    public Collection<Obra> consultarObras(Long idAutor) {
        return this.autorObraRepository.consultarObras(idAutor);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MensagemDTO atualizarVinculos(Autor autor, Collection<Long> idsObras) {
        this.autorObraRepository.deleteAllByAutor(autor);
        Collection<Obra> obras = this.obraRepository.findAllByIdIn(idsObras);
        Collection<AutorObra> autorObras = obras.stream().map(obra -> {
            AutorObra autorObra = new AutorObra();
            autorObra.setObra(obra);
            autorObra.setAutor(autor);
            return autorObra;
        }).collect(Collectors.toList());
        this.autorObraRepository.saveAll(autorObras);
        return null;
    }

    @Override
    public Page<ObraListagemDTO> consultarObrasPorFiltro(FiltroDTO filtroDTO, Pageable pageable) {
        Page<AutorObra> autorObras = this.autorObraRepository.findAll(this.buildSpecificationConsultaObras(filtroDTO), pageable);
        return new PageImpl<>(autorObras.stream()
                .map(autorObra -> ObraMapper.mapper(autorObra.getObra())).collect(Collectors.toList()));
    }

    @Override
    public Long consultarQuantidadeObras(FiltroDTO filtroDTO) {
        return this.autorObraRepository.count(this.buildSpecificationConsultaObras(filtroDTO));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void removerTodosPorObra(Obra obra) {
        this.autorObraRepository.deleteAllByObra(obra);
    }

    private Specification<AutorObra> buildSpecificationConsultaObras(FiltroDTO filtroDTO) {
        return (Specification<AutorObra>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filtroDTO.getNomeObra() != null) {
                predicates.add(criteriaBuilder.like(root.get("obra").get("nome"), "%".concat(filtroDTO.getNomeObra().concat("%"))));
            }
            if (filtroDTO.getDescricaoObra() != null) {
                predicates.add(criteriaBuilder.like(root.get("obra").get("descricao"), "%".concat(filtroDTO.getDescricaoObra().concat("%"))));
            }
            if (filtroDTO.getNomeAutor() != null) {
                predicates.add(criteriaBuilder.like(root.get("autor").get("nome"), "%".concat(filtroDTO.getNomeAutor()).concat("%")));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
