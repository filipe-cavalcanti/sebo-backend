package com.br.stefanini.sebo.infraestrutura.mensagem;

public class MensagemErro {
    public static final String EMAIL_EXISTE = "O e-mail já está cadastrado";
    public static final String NEMHUM_REGISTRO_ENCONTRADO = "Nemhum Registro encontrado";
    public static final String CPF_INCORRETO = "O cpf com formato incorreto ou já cadastrado";
    public static final String EXCLUSAO_AUTOR_NAO_PERMITIDA = "A exlusão do autor não pode ser completada, o autor tem obras cadastradas";
    public static final String ERRO_UPLOAD_IMAGEM = "Erro ao fazer o upload da imagem para a nuvem";
    public static final String ERRO_DATAS_OBRA = "As data de publicação e exposição não podem ser ambas nulas";
    public static final String ERRO_DOWNLOAD_IMAGEM = "Erro ao fazer o download da imagem";
}
