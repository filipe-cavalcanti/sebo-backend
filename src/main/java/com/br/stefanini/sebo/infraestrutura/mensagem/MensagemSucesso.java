package com.br.stefanini.sebo.infraestrutura.mensagem;

public class MensagemSucesso {
    public static final String CADASTRO_REALIZADO = "Cadastro realizado com sucesso";
    public static final String EDICAO_REALIZADA = "Edição realizada com sucesso";
    public static final String REMOCAO_REALIZADA = "Remoção realizada com sucesso";
}
