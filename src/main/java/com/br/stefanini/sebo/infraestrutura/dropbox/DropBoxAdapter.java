package com.br.stefanini.sebo.infraestrutura.dropbox;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Component
public class DropBoxAdapter {
    private DbxClientV2 client;
    private static final String ACCESS_TOKEN = "9Q_nkJbfZyAAAAAAAAAADBIwUoiNt3iVsXOpy7nXhnrx6BFmX-4jcpXbGcPYuWaz";
    private static final String PATH = "/SEBO/";
    private RandomString randomString = new RandomString(10);
    private static final String TYPE_FILE = ".png";

    public DropBoxAdapter() {
        this.createClient();
    }

    private void createClient() {
        DbxRequestConfig config = DbxRequestConfig.newBuilder("SEBO").build();
        this.client = new DbxClientV2(config, ACCESS_TOKEN);
    }

    public String upload(MultipartFile multipartFile) throws IOException, DbxException {
        String randomPrefix = this.randomString.nextString();
        this.client.files().uploadBuilder(PATH.concat(randomPrefix.concat(multipartFile.getName().concat(TYPE_FILE)))).uploadAndFinish(multipartFile.getInputStream());
        return randomPrefix.concat(multipartFile.getName().concat(TYPE_FILE));
    }

    public byte[] download(String fileName) throws IOException, DbxException {
        ByteArrayOutputStream fileOutputStream = new ByteArrayOutputStream();
        this.client.files().downloadBuilder(PATH.concat(fileName)).download(fileOutputStream);
        return fileOutputStream.toByteArray();
    }
}
