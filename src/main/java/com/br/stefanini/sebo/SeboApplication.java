package com.br.stefanini.sebo;

import com.br.stefanini.sebo.infraestrutura.filters.CORSFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SeboApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeboApplication.class, args);
	}
}
