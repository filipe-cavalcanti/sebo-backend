package com.br.stefanini.sebo.rest;

import com.br.stefanini.sebo.persistence.DTO.PaisSelecaoDTO;
import com.br.stefanini.sebo.service.declaration.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("paises")
public class PaisEndPoint {

    private PaisService paisService;

    @Autowired
    public PaisEndPoint(PaisService paisService) {
        this.paisService = paisService;
    }

    @GetMapping()
    public Collection<PaisSelecaoDTO> consultarParaSelecao() {
        return this.paisService.consultarParaSelecao();
    }
}
