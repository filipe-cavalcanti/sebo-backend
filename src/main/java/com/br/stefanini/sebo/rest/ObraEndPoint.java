package com.br.stefanini.sebo.rest;

import com.br.stefanini.sebo.persistence.DTO.FiltroDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraListagemDTO;
import com.br.stefanini.sebo.persistence.DTO.ObraSelecaoDTO;
import com.br.stefanini.sebo.service.declaration.ObraService;
import com.br.stefanini.sebo.service.mapper.ObraMapper;
import com.dropbox.core.DbxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;

@RestController
@RequestMapping("obras")
public class ObraEndPoint {

    private ObraService obraService;

    @Autowired
    public ObraEndPoint(ObraService obraService) {
        this.obraService = obraService;
    }

    @PostMapping()
    public MensagemDTO cadastrar(@RequestParam MultipartFile imagem, @RequestParam String nome,
                                 @RequestParam String descricao, @RequestParam String dataPublicacao,
                                 @RequestParam String dataExposicao, @RequestParam String autores) {
        return this.obraService.cadastrar(ObraMapper.mapper(imagem, nome, descricao, dataPublicacao, dataExposicao, autores));
    }

    @GetMapping()
    public Page<ObraListagemDTO> consultar(FiltroDTO filtroDTO, @RequestParam int pageIndex,
                                           @RequestParam(defaultValue = "10") int pageSize) {
        return this.obraService.consultar(filtroDTO, pageIndex, pageSize);
    }

    @GetMapping("selecao")
    public Collection<ObraSelecaoDTO> consultarParaSelecao(){
        return this.obraService.consultarParaSelecao();
    }

    @GetMapping("quantidade")
    public Long consultarQuantidade(FiltroDTO filtroDTO) {
        return this.obraService.consultarQuantidade(filtroDTO);
    }

    @GetMapping("{id}")
    public ObraDTO consultarPorID(@PathVariable("id") Long id) throws IOException, DbxException {
        return this.obraService.consultarPorId(id);
    }

    @GetMapping(value = "{id}/imagem", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity consultar(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.obraService.consultarImagem(id));
    }

    @PutMapping("{id}")
    public MensagemDTO editar(@RequestParam MultipartFile imagem, @RequestParam String nome,
                              @RequestParam String descricao, @RequestParam String dataPublicacao,
                              @RequestParam String dataExposicao, @RequestParam String autores, @PathVariable("id") Long id) {
        return this.obraService.editar(ObraMapper.mapper(imagem, nome, descricao, dataPublicacao, dataExposicao, autores), id);
    }

    @DeleteMapping("{id}")
    public MensagemDTO remover(@PathVariable("id") Long id) {
        return this.obraService.remover(id);
    }

}
