package com.br.stefanini.sebo.rest;

import com.br.stefanini.sebo.persistence.DTO.AutorDTO;
import com.br.stefanini.sebo.persistence.DTO.AutorListagemDTO;
import com.br.stefanini.sebo.persistence.DTO.AutorSelecaoDTO;
import com.br.stefanini.sebo.persistence.DTO.MensagemDTO;
import com.br.stefanini.sebo.service.declaration.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping(value = "/autores")
public class AutorEndPoint {

    private AutorService autorService;

    @Autowired
    public AutorEndPoint(AutorService autorService) {
        this.autorService = autorService;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MensagemDTO cadastrar(@RequestBody @Valid AutorDTO autorDTO) {
        return this.autorService.cadastrar(autorDTO);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MensagemDTO editar(@RequestBody @Valid AutorDTO autorDTO, @PathVariable("id") Long id) {
        return this.autorService.editar(autorDTO, id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MensagemDTO remover(@PathVariable("id") Long id) {
        return this.autorService.remover(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AutorDTO consultarPorId(@PathVariable("id") Long id) {
        return this.autorService.consultarPorId(id);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AutorListagemDTO> consultar() {
        return this.autorService.consultar();
    }

    @GetMapping("selecao")
    public Collection<AutorSelecaoDTO> consultarParaSelecao() {
        return this.autorService.consultarParaSelecao();
    }
}
